from flask import Flask, request, render_template
from mongokit import Connection
from models import * #TODO: set up models
import os, requests, urlparse, json, time, sys

app = Flask(__name__)

DEBUG = True
MONGOLAB_URI = os.environ['MONGOLAB_URI']
MONGODB_HOST = urlparse.urlparse(MONGOLAB_URI).geturl()
MONGODB_PORT = urlparse.urlparse(MONGOLAB_URI).port
DATABASE_NAME = urlparse.urlparse(MONGOLAB_URI).path[1:]

# ===========================================================================

globals = {'app_name': 'Boilerplate Application',
           'app_description': 'A web application.',
           'app_author': 'Kyri Group'
           }

@app.route('/')
def home():
    return render_template('home.html',
                           title='Home',
                           globals=globals)

@app.route('/about')
def about():
    return render_template('about.html',
                           title='About',
                           globals=globals)

# ===========================================================================

@app.route('/static/<directory>/<filename>')
def file(directory, filename):
    return app.send_static_file(directory+'/'+filename)

# ===========================================================================

if __name__ == "__main__":

    try:
        connection = Connection(MONGODB_HOST, MONGODB_PORT)
        database = connection[DATABASE_NAME]
    except:
        print "Unable to connect to database"
        #sys.exit(1)

    app.debug = DEBUG
    port = int(os.environ.get("PORT", 8080))
    app.run(host='0.0.0.0', port=port)
